package main

import (
	"fmt"
	"net/http"

	"throttler/sessions/manager"
	"throttler/sessions/store"
	"throttler/throttledmux"
	"time"
)

type handler struct{}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hi! This is a demo text.")
}

//A seample app starts up server to listen on http://localhost:3000.
//With default settings it should only handle 2 requests per 10 seconds for given browser/user session.
func main() {
	tmux, err := throttledmux.NewThrottledServeMux(2, time.Second*10)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	tmux.SessionManager = manager.NewCookieSessionManager()
	tmux.SessionsStore = store.NewSimpleInMemoryStore()
	tmux.Handle("/", new(handler))

	fmt.Println("Server is up and listening on localhost:3000")
	http.ListenAndServe(":3000", tmux)
}
