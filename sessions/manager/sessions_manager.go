package manager

import "net/http"

//SessionsManager is an interface describing object used to manage user sessions
type SessionsManager interface {
	//StartSession used to start a new sessions or reusing an existing
	StartSession(http.ResponseWriter, *http.Request) (string, error)
}
