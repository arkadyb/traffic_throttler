package manager

import (
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/http"

	"net/url"

	nethttp "net/http"
)

func TestGenerateSessionId(t *testing.T) {
	sid, err := generateSessionId()
	assert.NoError(t, err)
	assert.NotNil(t, sid)
	assert.Len(t, sid, 44)
}

func TestStartSession(t *testing.T) {
	manager := NewCookieSessionManager()
	assert.NotNil(t, manager)

	rw := &http.TestResponseWriter{}
	req := httptest.NewRequest("GET", "/", nil)

	t.Run("First session", func(t *testing.T) {
		sid, err := manager.StartSession(rw, req)
		assert.NoError(t, err)
		assert.NotNil(t, sid)

		cookiesHeader := rw.Header().Get("Set-Cookie")
		assert.NotNil(t, cookiesHeader)
		assert.Contains(t, cookiesHeader, url.QueryEscape(sid))

		req.AddCookie(&nethttp.Cookie{Name: "sessionid", Value: url.QueryEscape(sid), Path: "/"})
	})

	t.Run("Reuse existing session", func(t *testing.T) {
		sid, err := manager.StartSession(rw, req)
		assert.NoError(t, err)
		assert.NotNil(t, sid)

		cookie, err := req.Cookie("sessionid")
		if !assert.NoError(t, err) {
			t.FailNow()
		}

		if !assert.NotNil(t, cookie) {
			t.FailNow()
		}

		assert.Equal(t, url.QueryEscape(sid), cookie.Value)
	})
}
