package manager

import (
	"crypto/rand"
	"encoding/base64"
	"io"
	"net/http"
	"net/url"
	"sync"
)

//CookieSessionManager - implementation of SessionsManager interface to manage user sessions based on "sessionid" cookie value
type CookieSessionManager struct {
	lock       sync.Mutex
	cookieName string
}

//NewCookieSessionManager creates a new CookieSessionManager with default cookie name - sessionid
func NewCookieSessionManager() *CookieSessionManager {
	sm := CookieSessionManager{
		cookieName: "sessionid",
	}

	return &sm
}

//StartSession implements SessionsManager interface. Checks for sessionid cookie value in request and if found reuses this one or creates a new.
//Sets respective cookie value in response.
func (m *CookieSessionManager) StartSession(w http.ResponseWriter, r *http.Request) (string, error) {
	m.lock.Lock()
	defer m.lock.Unlock()

	var sid string
	cookie, err := r.Cookie(m.cookieName)
	if err != nil || cookie.Value == "" {
		sid, err = generateSessionId()
		if err != nil {
			return "", err
		}
		cookie := http.Cookie{Name: m.cookieName, Value: url.QueryEscape(sid), Path: "/"}
		http.SetCookie(w, &cookie)
	} else {
		sid, _ = url.QueryUnescape(cookie.Value)
	}

	return sid, nil
}

//generateSessionId used to generate randome 32 byte length session id hash
func generateSessionId() (string, error) {
	b := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), nil
}
