package store

import (
	"errors"
	"time"
)

//SimpleInMemoryStore represents a very simple in-memory storage for user sessions
type SimpleInMemoryStore struct {
	sessions map[string][]time.Time
}

//Add implements SessionsStore interface. Used to add information about new user session or updating existing session time table
func (s *SimpleInMemoryStore) Add(sid string) error {
	if sid == "" {
		return errors.New("Session Id value cant be empty")
	}

	if ts, ok := s.sessions[sid]; ok {
		s.sessions[sid] = append(ts, time.Now())
	} else {
		s.sessions[sid] = []time.Time{time.Now()}
	}

	return nil
}

//Get implements SessionsStore interface.
//Returns a number of recorded user requests with given session id registered in specified period
func (s *SimpleInMemoryStore) Get(sid string, period time.Duration) (int, error) {
	now := time.Now()
	res := 0

	if ts, ok := s.sessions[sid]; ok {
		for i := len(ts) - 1; i >= 0; i-- {
			if ts[i].After(now.Add(-period)) {
				res++
			} else {
				break
			}
		}
	}

	return res, nil
}

//NewSimpleInMemoryStore creates a new SimpleInMemoryStore object
func NewSimpleInMemoryStore() *SimpleInMemoryStore {
	st := new(SimpleInMemoryStore)
	st.sessions = make(map[string][]time.Time)

	return st
}
