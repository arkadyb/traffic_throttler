package store

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestAdd(t *testing.T) {

	t.Run("Session id = 1; No error", func(t *testing.T) {
		store := NewSimpleInMemoryStore()
		assert.NotNil(t, store)

		err := store.Add("1")
		assert.NoError(t, err)

		if assert.NotNil(t, store.sessions["1"]) {
			assert.Len(t, store.sessions["1"], 1)
		}
	})

	t.Run("2 records for session id = 1; No error", func(t *testing.T) {
		store := NewSimpleInMemoryStore()
		assert.NotNil(t, store)

		for i := 0; i < 2; i++ {
			err := store.Add("1")
			assert.NoError(t, err)
		}

		if assert.NotNil(t, store.sessions["1"]) {
			assert.Len(t, store.sessions["1"], 2)
		}
	})

	t.Run("2 records for 2 session id`s (1,2); No error", func(t *testing.T) {
		store := NewSimpleInMemoryStore()
		assert.NotNil(t, store)

		var err error
		for i := 0; i < 2; i++ {
			err = store.Add("1")
			assert.NoError(t, err)

			err = store.Add("2")
			assert.NoError(t, err)
		}
		assert.Len(t, store.sessions, 2)

		if assert.NotNil(t, store.sessions["1"]) {
			assert.Len(t, store.sessions["1"], 2)
		}

		if assert.NotNil(t, store.sessions["2"]) {
			assert.Len(t, store.sessions["2"], 2)
		}
	})

	t.Run("Empty session id; Error", func(t *testing.T) {
		store := NewSimpleInMemoryStore()
		assert.NotNil(t, store)

		err := store.Add("")
		assert.Error(t, err)
		assert.Len(t, store.sessions, 0)
	})
}

func TestGet(t *testing.T) {
	t.Run("Empty sore", func(t *testing.T) {
		store := NewSimpleInMemoryStore()
		assert.NotNil(t, store)

		n, err := store.Get("1", time.Minute*1)
		assert.NoError(t, err)
		assert.Equal(t, n, 0)
	})

	t.Run("2 items for id = 1", func(t *testing.T) {
		store := NewSimpleInMemoryStore()
		assert.NotNil(t, store)

		store.Add("1")
		store.Add("1")

		n, err := store.Get("1", time.Minute*1)
		assert.NoError(t, err)

		assert.Equal(t, n, 2)
	})
}
