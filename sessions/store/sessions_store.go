package store

import "time"

//SessionsStore is an interface describing object used to store and read iformation about user sessions
type SessionsStore interface {
	//Add`s a new request timestamp into store
	Add(string) error

	//Get returns a number of registered requests for given session id in specified period
	Get(string, time.Duration) (int, error)
}
