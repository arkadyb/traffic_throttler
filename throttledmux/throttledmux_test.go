package throttledmux

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"throttler/sessions/manager"
	"throttler/sessions/store"
	"time"

	"github.com/stretchr/testify/assert"
)

var handlercalls int

type handler struct{}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	handlercalls++
}

func TestNewThrottledServeMux(t *testing.T) {

	tests := []struct {
		name          string
		maxrequests   int
		period        time.Duration
		errorexpected bool
	}{
		{name: "Ok1", maxrequests: 2, period: time.Second * 10, errorexpected: false},
		{name: "Ok2", maxrequests: 200, period: time.Minute * 10, errorexpected: false},
		{name: "Negative maxrequests", maxrequests: -1, period: 0, errorexpected: true},
		{name: "Duration not set", maxrequests: 2, period: 0, errorexpected: true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mux, err := NewThrottledServeMux(tt.maxrequests, tt.period)
			if err != nil && !tt.errorexpected {
				t.Error(err)
			} else if err == nil {
				assert.NotNil(t, mux)
				assert.Equal(t, mux.maxRequests, tt.maxrequests)
				assert.Equal(t, mux.duration, tt.period)
			}
		})
	}
}

func TestHandle(t *testing.T) {
	tmux, err := NewThrottledServeMux(2, time.Second*5)
	assert.NotNil(t, tmux)
	assert.NoError(t, err)

	tmux.SessionManager = manager.NewCookieSessionManager()
	assert.NotNil(t, tmux.SessionManager)

	tmux.SessionsStore = store.NewSimpleInMemoryStore()
	assert.NotNil(t, tmux.SessionsStore)

	req, err := http.NewRequest("GET", "/", nil)
	req.AddCookie(&http.Cookie{Name: "sessionid", Value: url.QueryEscape("1"), Path: "/"})
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	assert.NotNil(t, rr)

	hFunc := tmux.throttlingFunc(new(handler))
	var successes, failures int = 0, 0
	for i := 0; i < 5; i++ {
		hFunc.ServeHTTP(rr, req)
		if rr.Code != http.StatusOK {
			failures++
		} else {
			successes++
		}
	}

	assert.Equal(t, 3, failures)
	assert.Equal(t, 2, successes)
	assert.Equal(t, successes, handlercalls)
}
