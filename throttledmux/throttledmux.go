package throttledmux

import (
	"net/http"
	"throttler/sessions/manager"
	"throttler/sessions/store"

	"errors"
	"fmt"
	"time"
)

//ThrottledServeMux is multiplexer with request throttling support
type ThrottledServeMux struct {
	http.ServeMux

	duration    time.Duration
	maxRequests int

	//SessionManager used to create/reuse user sessions
	SessionManager manager.SessionsManager

	//SessionsStore used to save infmartion about user sessions/requests
	SessionsStore store.SessionsStore
}

//Handle maps given pattern with handler
func (tm *ThrottledServeMux) Handle(pattern string, handler http.Handler) {
	if tm.SessionManager == nil {
		panic("SessionManager is required for throttled multiplexer")
	}

	if tm.SessionsStore == nil {
		panic("SessionsStore is required for throttled multiplexer")
	}

	tm.ServeMux.Handle(pattern, tm.throttlingFunc(handler))
}

//throttlingFunc wraps given handler into throttled processing.
//Function uses SessionManager to find out data about user sessions (or creates a new one if nothin found).
//With the session data it verifies space availability to run the handler using SessionsStore.
//In case no more space available, it returns an error -  http 500.
func (tm *ThrottledServeMux) throttlingFunc(h http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		sid, err := tm.SessionManager.StartSession(rw, r)
		if err != nil {
			http.Error(rw, err.Error(), 500)
			return
		}

		if tm.maxRequests > 0 { //for 0 value no limits
			nSessions, err := tm.SessionsStore.Get(sid, tm.duration)
			if err != nil {
				http.Error(rw, err.Error(), 500)
				return
			}

			if nSessions >= tm.maxRequests {
				http.Error(rw, "Requests limit number has been reached", 500)
				return
			}
		}

		tm.SessionsStore.Add(sid)
		h.ServeHTTP(rw, r)
	})
}

//NewThrottledServeMux creates a new NewThrottledServeMux instance
func NewThrottledServeMux(maxrequests int, per time.Duration) (*ThrottledServeMux, error) {
	if maxrequests < 0 {
		return nil, errors.New("MaxRequests argument must be positive value")
	}

	if maxrequests > 0 && per.Nanoseconds() == 0 {
		return nil, fmt.Errorf("For given MaxRequests argument value - %d, duration must be bigger than 0", maxrequests)
	}

	mux := new(ThrottledServeMux)

	mux.maxRequests = maxrequests
	mux.duration = per

	return mux, nil
}
